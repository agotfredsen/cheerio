package com.example.alexander.cheerio;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Alexander on 30/09/16.
 */
public class CheerioApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

    }
}
