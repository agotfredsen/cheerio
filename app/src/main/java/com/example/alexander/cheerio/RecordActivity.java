package com.example.alexander.cheerio;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RecordActivity extends AppCompatActivity {

    private Realm myRealm;

    /// RadioButton radioFail; <- Do this to make it available in all the methods (also delete "final RadioButton")

    RadioButton radioFail;
    RadioButton radioSuccess;
    Button buttonRecord;
    ImageView img;
    EditText recordText;
    RecordObject record1;
    String recordTypeString;
    Intent goToList;

    // Didn't work with onClickListener

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        myRealm  = Realm.getInstance(config);

        //     Changed from    myRealm = Realm.getDefaultInstance();


        img = (ImageView) findViewById(R.id.recordImage);
        img.setImageResource(R.drawable.success);

        radioFail = (RadioButton) findViewById(R.id.radioFail);
        radioSuccess = (RadioButton) findViewById(R.id.radioSuccess);
        buttonRecord = (Button) findViewById(R.id.recordButton);

        radioSuccess.setOnClickListener(radio_listener1);
        radioFail.setOnClickListener(radio_listener2);
        buttonRecord.setOnClickListener(recordListener);

    }

    private View.OnClickListener radio_listener1 = new View.OnClickListener() {
        public void onClick(View v) {
            img.setImageResource(R.drawable.success);

        }
    };

    private View.OnClickListener radio_listener2 = new View.OnClickListener() {
        public void onClick(View v) {
            img.setImageResource(R.drawable.fail);
        }
    };

    private View.OnClickListener recordListener = new View.OnClickListener() {
        public void onClick(View v) {

            recordText = (EditText) findViewById(R.id.recordText);


            recordTypeString = "";
            if (radioSuccess.isChecked()) {
                recordTypeString = "Success";
            }
            else if (radioFail.isChecked()) {
                recordTypeString = "Fail";
            }

            myRealm.beginTransaction();

            record1 = myRealm.createObject(RecordObject.class);

            record1.setRecordType(recordTypeString);
            record1.setExplanationText(recordText.getText().toString());
            record1.setId(RecordObject.getIdCount()+1);

            myRealm.commitTransaction();

            goToList = new Intent(RecordActivity.this, ListRecordsActivity.class);
            startActivity(goToList);

        }
    };



}
