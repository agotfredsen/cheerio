package com.example.alexander.cheerio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by Alexander on 26/09/16.
 */
public class ListRecordsActivity extends AppCompatActivity {

    private Realm myRealm;
    RecyclerView listRecyclerView;
    RecyclerView.Adapter listAdapter;
    RecyclerView.LayoutManager listLayoutManager;
    RealmResults<RecordObject> results1;
    List<RecordObject> mDataset;
    RecordObject recordObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_records);
        listRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        listRecyclerView.setHasFixedSize(true);

        listLayoutManager = new GridLayoutManager(this, 2);
        listRecyclerView.setLayoutManager(listLayoutManager);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        // Is this necessary twice, both here and in RecordActivity?

        myRealm = Realm.getInstance(config);
        mDataset = myRealm.where(RecordObject.class).findAll();

        listAdapter = new RecyclerViewAdapter(mDataset, new RecyclerViewAdapter.OnItemClickListener() {
            @Override public void onItemClick(RecordObject item, int position) {

                Intent goToSingleRecord = new Intent(ListRecordsActivity.this, ViewRecordActivity.class);
                goToSingleRecord.putExtra("itemId", mDataset.get(position).getId());
                startActivity(goToSingleRecord);

            }
        });

        listRecyclerView.setAdapter(listAdapter);



    }



}