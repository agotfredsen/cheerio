package com.example.alexander.cheerio;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexander on 26/09/16.
 */
public class RecordObject extends RealmObject implements Serializable{

    private String explanationText;
    private String recordType;
    private int id;
    private static int idCount = 0;

    public RecordObject () {
        id = ++idCount;
    }

    public String getExplanationText() {
        return explanationText;
    }
    public void setExplanationText(String explanationText) {this.explanationText = explanationText;}

    public String getRecordType() {
        return recordType;
    }
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }


    public int getId() {return id;}
    public void setId(int id) {this.id = id;}


    public static int getIdCount() {
        return idCount;
    }

}
