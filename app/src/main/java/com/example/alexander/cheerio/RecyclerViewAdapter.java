package com.example.alexander.cheerio;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 08/10/16.
 */
    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

   public interface OnItemClickListener{
       void onItemClick(RecordObject item, int position);
   }

   private List<RecordObject> mDataset = new ArrayList<>();
    private OnItemClickListener listener;

    public RecyclerViewAdapter(List<RecordObject> mDataset, OnItemClickListener listener) {
        this.mDataset = mDataset;
        this.listener = listener;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description;
        public ImageView image;
        public int itemId;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.gridTitle);
            description = (TextView) view.findViewById(R.id.gridDescription);
            image = (ImageView) view.findViewById(R.id.gridImage);
        }

        public void bind(final RecordObject item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, getAdapterPosition());
                }
            });
        }

    }


    public RecyclerViewAdapter(List<RecordObject> mDataset) {
        if (mDataset != null) {
            this.mDataset = mDataset;
        }
        else {
            this.mDataset = new ArrayList<>();
        }

        //Shorter: this.mDataset = mDataset != null ? mDataset : new ArrayList<RecordObject>();
    }


    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item_view, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    RecordObject recordObject = mDataset.get(position);
        holder.title.setText(recordObject.getRecordType());
        holder.description.setText(recordObject.getExplanationText());

        holder.bind(mDataset.get(position), listener);


        if (recordObject.getRecordType().equals("Success")) {
            holder.image.setImageResource(R.drawable.success);
        }
        else {
            holder.image.setImageResource(R.drawable.fail);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
