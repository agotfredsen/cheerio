package com.example.alexander.cheerio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Alexander on 03/11/16.
 */
public class ViewRecordActivity extends AppCompatActivity {

    private Realm myRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_record);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        myRealm  = Realm.getInstance(config);


        Intent test = getIntent();
        int intValue = test.getIntExtra("itemId", 0);


        // Build the query looking at all users:
        RealmQuery<RecordObject> query = myRealm.where(RecordObject.class);

    // Add query conditions:
        query.equalTo("id", intValue);

    // Execute the query:
        RealmResults<RecordObject> result1 = query.findAll();

        for (RecordObject ro : result1) {
            ro.getExplanationText();
            ro.getRecordType();

            TextView explanation = (TextView) findViewById(R.id.viewExplanationText);
            explanation.setText(ro.getExplanationText());

            TextView type = (TextView) findViewById(R.id.viewRecordType);
            type.setText (ro.getRecordType());


            ImageView image = (ImageView) findViewById(R.id.viewRecordImage);

            if (ro.getRecordType().equals("Success")) {
                image.setImageResource(R.drawable.success);
            }
            else {
                image.setImageResource(R.drawable.fail);
            }


        }





       // TextView text = (TextView) findViewById(R.id.test);
       // text.setText(String.valueOf(intValue));


        }



    }


